"use strict"
const express = require('express')
const restClientConstructor = require('node-rest-client').Client
const bodyParser = require('body-parser')
const getPosts = require('./getPosts.js')
const mongoose = require('mongoose')

mongoose.connect('mongodb://127.0.0.1:27017/test')
let app = express()
let Data = require('./models/schema')
let restClient = new restClientConstructor()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.type("text/html")
    res.send("<p>Create posts request</p>" +
        "<form method='post' action='/add'>" +
        "<input type='text' name='sources'>" +
        "<button type='submit'>Submit</button>" +
        "</form>" +
        "<p>Recive posts by ID</p>" +
        "<form method='post' action='/getPosts'>" +
        "<input type='text' name='ID'>" +
        "<button type='submit'>Submit</button>" +
        "</form>" +
        "<p>Delete record from BD by ID</p>" +
        "<form method='post' action='/deleteDBRec'>" +
        "<input type='text' name='ID'>" +
        "<button type='submit'>Submit</button>" +
        "</form>" + "<a href='/showAllDB'> Show All DB </a>" + "</br>"
		+ "<a href='/delAllDBRecs'> Del All DB Rec's </a>")
})

app.post('/add', (req, res) => {
    let sources = req.body.sources.split(',')
    let data = new Data({ sources: sources})
    data.save();
    res.send(data.id)
})

app.get('/showDB', (req, res) => {
    Data.find({}, (err, data) => {
        res.json(data)
    })
})

app.get('/showAllDB',(req, res) => {
	Data.find({}, (err, result) => {
		if(err) throw err;
			res.json(result.map(a => {
				return {
					"id":a["_id"],
					"srcs":a.sources
				}
			}))
		})
})

app.post('/deleteDBRec',(req, res) => {
	let ID = req.body.ID
	Data.find({_id:ID}, (err, result) => {
		if (err) throw (err)
		result[0].remove(err => {
			if (err) throw err;
			res.send("<p>"+"Record w\\ ID:"+ID+" deleted"+"</p>"+ "<a href='/'> To Main </a>")
		})
	})
})

app.post('/getPosts', (req, res) => {
    let ID = req.body.ID
    Data.find({_id:ID}, (err, result) => {
		let sources = result[0].sources
		getPosts.getSortedPosts(sources,100).then(data => {
			res.json(data)
		})
    })
})

app.get('/delAllDBRecs',(req, res) => {
let deleteRecs = new Promise(function(resolve, reject) {
		Data.find({}, (err, results) => {
			results.forEach(rec => {
				rec.remove(err => {if (err) throw err})
			})
		})
		resolve()
	})
deleteRecs.then(res.send("DB Clear"))
})

app.listen('3000', () => {
    console.log('Started at port 3000')
})
