"use strict"
let Client = require('node-rest-client').Client
let client = new Client()
let querystring = require("querystring")
let url = require('url')
const max_posts = 100;

let byPopularity = (a,b) => {
	if (a.popularity < b.popularity) return 1
	if (a.popularity === b.popularity) return 0
	if (a.popularity > b.popularity) return -1
}

let getSortedPosts = (sources) => {
	return new Promise(function(resolve, reject) {
		getAllPostsWithPopul(sources).then(
			postsList => {
				resolve(
					postsList.sort(byPopularity)
				)
		})
	})
}

let getAllPostsWithPopul = (sources) => {
return new Promise(function(resolve, reject) {
	Promise.all(
		sources.map(src => getPostsWithPopul(src))
	).then(
		(data) => {
			resolve(
				data.reduce(
					(finArr, arr) => {
						return finArr.concat(arr)
					},[]
				)
			) }
	)
});
}

let getPostsWithPopul = (src) => {
return new Promise(function(resolve, reject) {
	client.get("https://api.vk.com/method/wall.get?domain="+src+"&count=1&v=5.57",(data) => {
		let groupID = data.response.items[0].owner_id * -1
		client.get("https://api.vk.com/method/groups.getMembers?group_id="+groupID+"&count=0&v=5.57", data => {
			let groupMembersCount = data.response.count
			getAllPostsFromSource(src).then( data => {
				resolve(data.map(post => {
					post['popularity'] = post.reposts.count / groupMembersCount
					return post
				}))
			})
		})

	})
})
}

let getAllPostsFromSource = (src) => {
return new Promise(function(resolve, reject) {
		client.get("https://api.vk.com/method/wall.get?domain="+src+"&count=1&v=5.57",(data) => {
			let postsCount = data.response.count
			if (postsCount < 100){
				getPostsFromSource(src,0,100).then(data => {
					resolve(data)
				})
			} else {
				if (postsCount > max_posts) {postsCount = max_posts}
				let postsRanges = rangeBy100(postsCount).map(range => {
					range['domain'] = src
					return range
				})
				Promise.all(
					postsRanges.map(range => {
					return getPostsFromSource(range.domain, range.offset, range.count)
				}))
				.then(data => {
					resolve(data.reduce((finArr, arr) => {
						return finArr.concat(arr)
					},[]))
				})

			}
		})
	})
}

function getPostsFromSource(domain,offset,count)  {
let args = {
	domain:arguments[0],
	offset:arguments[1],
	count:arguments[2]
}
 return	new Promise(function(resolve, reject) {
		client.get("https://api.vk.com/method/wall.get?"+querystring.encode(args)+"&v=5.57", (data) => {
			resolve(data.response.items)
		})
	});
}

let rangeBy100 = (a) => {
	let num = (a / 100).toFixed()
	let fracPart = a % 100
	let arr = [];
	for (var i = 0; i < num; i++) {
		arr.push({
			offset:i*100,
			count:100
		})
	}
	arr.push({
		offset:i*100,
		count:fracPart
	})
	return arr
}

exports.getPostsFromSource = getPostsFromSource
exports.getAllPostsFromSource = getAllPostsFromSource
exports.getPostsWithPopul = getPostsWithPopul
exports.getSortedPosts = getSortedPosts
exports.getAllPostsWithPopul = getAllPostsWithPopul
